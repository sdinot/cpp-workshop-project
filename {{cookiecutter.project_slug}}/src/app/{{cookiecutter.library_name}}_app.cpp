#include "{{ cookiecutter.library_name }}/{{ cookiecutter.library_name }}.hpp"
#include <iostream>

int main(int argc, char **argv) {
  int result = {{ cookiecutter.library_namespace }}::add_one(1);
  std::cout << "1 + 1 = " << result << '\n';
}
