#include "{{ cookiecutter.library_name }}/{{ cookiecutter.library_name }}.hpp"

namespace {{ cookiecutter.library_namespace }} {

int add_one(int x) {
  return x + 1;
}

} // namespace {{ cookiecutter.library_namespace }}
