add_executable(tests tests.cpp {{ cookiecutter.library_name }}_test.cpp)
target_link_libraries(tests PUBLIC {{ cookiecutter.library_name }} doctest::doctest)
