#include "{{ cookiecutter.library_name }}/{{ cookiecutter.library_name }}.hpp"
#include <doctest/doctest.h>

TEST_CASE( "add_one" ) {
  REQUIRE( {{ cookiecutter.library_namespace }}::add_one(0) == 1 );
  REQUIRE( {{ cookiecutter.library_namespace }}::add_one(123) == 124 );
  REQUIRE( {{ cookiecutter.library_namespace }}::add_one(-1) == 0 );
}
