# Welcome to {{ cookiecutter.project_name }}

{% if cookiecutter.project_license == "GPL-3.0-or-later" or
      cookiecutter.project_license == "LGPL-3.0-or-later" -%}
![License: GNU {{ cookiecutter.project_license|replace('-',' ') }}](https://img.shields.io/badge/License-GNU_{{ cookiecutter.project_license|replace('-','_') }}-yellow.svg)
{%- else -%}
![License: {{ cookiecutter.project_license|replace('-',' ') }}](https://img.shields.io/badge/License-{{ cookiecutter.project_license|replace('-','_') }}-yellow.svg)
{%- endif %}

![C++ logo](resources/cpp_logo.png)

# Prerequisites

Building {{ cookiecutter.project_name }} requires the following software installed:

* A C++20-compliant compiler
* CMake `>= 3.9`
* Doxygen (optional, documentation building is skipped if missing)
* The testing framework [Catch2](https://github.com/catchorg/Catch2) for
  building the test suite

# Building {{ cookiecutter.project_name }}

The following sequence of commands builds {{ cookiecutter.project_name }}. It assumes that your current
working directory is the top-level directory of the freshly cloned repository:

```bash
$ cmake -S . -B build -DCMAKE_BUILD_TYPE=RelWithDebInfo
$ cmake --build build
```

The build process can be customized with the following CMake variables, which
can be set by adding `-D<var>={ON, OFF}` to the `cmake` call:

* `BUILD_TESTING`: Enable building of the test suite (default: `ON`)
* `BUILD_DOCS`: Enable building the documentation (default: `ON`)

# Testing {{ cookiecutter.project_name }}

When built according to the above explanation (with `-DBUILD_TESTING=ON`), the
C++ test suite of {{ cookiecutter.project_name }} can be run from the top-level directory:

```bash
$ ./build/tests/tests
```

# Documentation

{{ cookiecutter.project_name }} provides a Doxygen documentation. You can build the documentation locally
by making sure that Doxygen is installed on your system and running this command
from the top-level build directory:

```bash
$ cmake --build build --target doxygen
```

The web documentation can then be browsed by opening `build/doc/html/index.html`
in your browser.

# Copyright and license

Copyright (c) {% now 'local', '%Y' %}, {{ cookiecutter.copyright_holder_name }} - {{ cookiecutter.copyright_holder_website }}

{% if cookiecutter.project_license == "GPL-3.0-or-later" -%}
{{ cookiecutter.project_name }} is licensed under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3
of the License, or (at your option) any later version. See [COPYING](COPYING)
file for more information.
{% elif cookiecutter.project_license == "LGPL-3.0-or-later" -%}
{{ cookiecutter.project_name }} is licensed under the terms of the GNU Lesser
General Public License as published by the Free Software Foundation, either
version 3 of the License, or (at your option) any later version. See
[COPYING.LESSER](COPYING.LESSER) and [COPYING](COPYING) files for more
information.
{%- else -%}
{{ cookiecutter.project_name }} is licensed under the terms of the
{{ cookiecutter.project_license|replace('-',' ') }} license. See
[LICENSE](LICENSE) file for more information.
{%- endif %}
