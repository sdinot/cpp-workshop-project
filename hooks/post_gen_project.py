#!/usr/bin/env python

import json
import os
import shutil


def set_project_license(project_license):

    if project_license == "GPL-3.0-or-later":
        # If the license selected for the project is the GNU GPL v3.0, it is
        # customary to name the file that contains this license "COPYING"
        shutil.move("licenses/LICENSE.GPL-3.0-or-later", "COPYING")
    elif project_license == "LGPL-3.0-or-later":
        # Since the GNU LGPL v3.0 is an exception to the GNU GPL v3.0 without
        # incorporating its text, it is necessary to keep both files. These
        # are usually named "COPYING" and "COPYING.LESSER".
        shutil.move("licenses/LICENSE.LGPL-3.0-or-later", "COPYING.LESSER")
        shutil.move("licenses/LICENSE.GPL-3.0-or-later", "COPYING")
    else:
        # Otherwise, the license file is usually named "LICENSE"
        os.rename(f"licenses/LICENSE.{project_license}", "LICENSE")

    # Remove irrelevant license files
    shutil.rmtree('licenses')


def main():
    """
    Entry point of the epilogue script.

    This script is executed after the generation of the skeleton of your
    C++ project.
    """
    # The "cookiecutter" variable contains the list of selected options and
    # some additional information. Its content is therefore different from
    # that of the initial "cookiecutter.json" file. The content of this
    # variable can be displayed with the following instruction:
    print("""{{cookiecutter|jsonify}}""")
    workdir = os.getcwd()
    cookiecutter_json = "{{cookiecutter._template}}/cookiecutter.json"
    project_name = "{{cookiecutter.project_name}}"
    project_slug = "{{cookiecutter.project_slug}}"
    library_name = "{{cookiecutter.library_name}}"
    project_dir = os.path.realpath(os.path.curdir)
    project_license = "{{cookiecutter.project_license}}"
    print("Cookiecutter JSON file: {}".format(cookiecutter_json))
    print("Working directory:      {}".format(workdir))
    print("Project directory:      {}".format(project_dir))
    print("Project name:           {}".format(project_name))
    print("Project slug:           {}".format(project_slug))
    print("Project license:        {}".format(project_license))
    print("Library name:           {}".format(library_name))
    set_project_license(project_license)


if __name__ == "__main__":
    main()
