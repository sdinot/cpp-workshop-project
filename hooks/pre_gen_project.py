#!/usr/bin/env python

# import os
# import subprocess

# workdir = os.getcwd()
# print("Working directory: {}".format(workdir))
# result = subprocess.run("/usr/bin/ls -la {}".format(workdir).split(),
#                         capture_output=True, text=True)
# print("stdout:", result.stdout)
# print("stderr:", result.stderr)

import re
import sys

PROJECT_NAME_REGEX = r"^[a-zA-Z][-'_+ a-zA-Z0-9]+$"
project_name = "{{cookiecutter.project_name}}"
if not re.match(PROJECT_NAME_REGEX, project_name):
    print(r"""ERROR: The project name "{}" is not a valid C++ project name.""".format(project_name))
    print(r"""The name must begin with a letter. Allowed characters are letters, numbers, "-", "_", "+", "'" and space.""")
    # Exit to cancel project
    sys.exit(1)

PROJECT_SLUG_REGEX = r"^[a-z][-a-z0-9]+$"
project_slug = "{{cookiecutter.project_slug}}"
if not re.match(PROJECT_SLUG_REGEX, project_slug):
    print(r"""ERROR: The project slug "{}" is not valid.""".format(project_slug))
    print(r"""The slug must begin with a lowercase letter. Allowed characters are lowercase letters, numbers and "-".""")
    # Exit to cancel project
    sys.exit(1)
