# C++ Workshop Project

This project was created to illustrate a workshop on using Cookiecutter to
create parameterized project templates. It has no other ambition.

# Copyright and license

Copyright (c) 2023, Sébastien Dinot - https://www.palabritudes.net

This project is licensed under the terms of the MIT license. See
[LICENSE](LICENSE) file for more information.
